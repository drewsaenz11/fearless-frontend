import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            location: '',
            locations: []
        };
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this)
        this.handleEndsChange = this.handleEndsChange.bind(this)
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this)
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
    }
    
    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
        
        const response = await fetch(url);
        
        if (response.ok) {
            let data = await response.json();
            // console.log(data)
            this.setState({locations: data.locations})
        }
        // console.log(this.componentDidMount)
        // console.log(this.state)
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            this.setState({
                name: '',
                starts: '',
                ends:'',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location:'',
            });
        }
    }
    
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    
    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }
    
    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({maxPresentations: value})
    }
    
    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({maxAttendees: value})
    }
    
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }
    
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" id="name" name="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input type="date" value={this.state.starts} onChange={this.handleStartsChange} placeholder="" required name="starts" id="starts" className="form-control" />
                                <label htmlFor="starts">Starts</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input value={this.state.ends} onChange={this.handleEndsChange} placeholder="" required type="date" name="ends" id="ends" className="form-control" />
                                <label htmlFor="ends">Ends</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input value={this.state.description} onChange={this.handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" className="form-control" />
                                <label htmlFor="ends">Description</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input value={this.state.maxPresentations} onChange={this.handleMaxPresentationsChange} placeholder="" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                                <label htmlFor="ends">Maximum Presentations</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input value={this.state.maxAttendees} onChange={this.handleMaxAttendeesChange} placeholder="" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                                <label htmlFor="ends">Maximum Attendees</label>
                            </div>

                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                                    <option>Choose a location</option>
                                    {this.state.locations?.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ConferenceForm;