function createCard(name, description, pictureUrl, starts, ends, location) {
    console.log('createCard()', name, description, pictureUrl)
    return `
        <div class="card" style="border: 0px; ">
        <div class="shadow-sm p-3 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            ${starts} - ${ends}
            </div>
        </div>
        </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
    
        if (!response.ok) {
            console.log("error:the response is bad")
        } else {
            const data = await response.json();
            let columnNum = 0
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const location = details.conference.location.name;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const columns = document.querySelectorAll(".col");
                    let column = columns[columnNum];
                    columnNum += 1;
                    if (columnNum > 2) {
                        columnNum = 0
                    }
                    column.innerHTML += html;
                    // console.log(html);
                    // console.log(details)
                }
            }
        }
    } catch (e) {
        console.log("error:the response is bad")
    }
});